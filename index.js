const express = require('express');
const configs = require('./package.json'); // Adicionando o .json como objeto = 'configs'
const teste = require('./erro.json');

const srv = express();

// Criando Map 'ceps'
const ceps = new Map();

ceps.set('12345', {
    log: 'R. Antonio da silva',
    bairro: 'Vicentina',
    uf: 'RS'
});

ceps.set('678910', {
    log: 'R. Joaquim',
    bairro: 'Centro',
    uf: 'RS'
});

ceps.set('54321', {
    log: 'R. São João',
    bairro: 'Fião',
    uf: 'RS'
});

srv.get('/cep/:numCep', (req, res)=>{
    const cepDigitado = ceps.get(req.params.numCep);
    res.json(cepDigitado);
});

// Fim da aplicação Map 'ceps'

/*
srv.get('/:nome', (req, res)=>{
    res.send(`Oi, ${req.params.nome}`)
}); // Através da url, digitando algum nome como parâmetro
*/

srv.get('/inicio', getInicio); // Chama o função getInicio

srv.get('/', (req, res)=>{
    res.json(
        {
            nome: configs.name,
            versao: configs.version,
            licenca: configs.license
        }
    );
}); // Consultando o JSON

srv.get('/erro', (req, res)=>{
    res.json(
        {
        erro: teste.erro,
        mensagem: teste.mensagem
        }
    )
});



function getInicio(pedido, resposta) { // (req, res)
    resposta.send('Recebi pedido p/ inicio');
}

srv.listen(3030, ()=>{ // <- função anônima aqui
    console.log('Pronto para rodar. Aguardando conexões...');
});